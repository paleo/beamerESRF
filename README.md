# ESRF beamer template

This beamer template is inspired from the official ESRF powerpoint template.

## Contents

* `template.tex` : main file where you write the content
* `templateUtils.tex` : contains the template design and the compilation variables
* Images `ESRF_logo.png` and `ESRF_logo_bottom.png`

## Usage

This template is not a `.cls` class. You will have to copy the two `.tex` files and the two images for each new presentation.

See the beginning of the file `templateUtils.tex` to define custom commands and compilation options.




